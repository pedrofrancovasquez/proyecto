package cl.sti.loginfacebook;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class PrimerInicioActivity extends AppCompatActivity {

    private Button btnCrearCuenta;
    private Button btnEntrar;
    private Button btnTestMenu;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_primer_inicio);
        btnCrearCuenta = (Button)findViewById(R.id.btnCrearCuenta);
        btnEntrar = (Button)findViewById(R.id.btnEntrar);
        btnTestMenu = (Button)findViewById(R.id.btnTestMenu);
       ;

        btnCrearCuenta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                crearcuenta();
            }
        });
        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                entrar();
            }
        });
        btnTestMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                testmenu();
            }
        });


    }

    private void crearcuenta(){
        Intent i = new Intent(this,MainActivity.class);
        startActivity(i);
    }

    private void entrar(){
        Intent i = new Intent(this,LoginActivity.class);
        startActivity(i);
    }

    private void testmenu(){
        Intent i = new Intent(this,SeleccionActivity.class);
        startActivity(i);
    }



}
