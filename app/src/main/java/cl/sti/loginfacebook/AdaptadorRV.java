package cl.sti.loginfacebook;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by opi on 01-06-2016.
 */
public class AdaptadorRV extends  RecyclerView.Adapter<AdaptadorRV.ViewHolder>{

    private final Context context;
    private final ArrayList<Locales> dataSet;

    public AdaptadorRV(Context context, ArrayList<Locales> dataSet){
        this.context = context;
        this.dataSet = dataSet;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.contenidorv, viewGroup, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {

        final Locales locales = dataSet.get(i);
        try {
            Picasso.with(context).load(locales.getUrlImagen()).into(viewHolder.imgPreview);

            viewHolder.txtTitulo.setText(locales.getTitulo());
            viewHolder.txtDescripcion.setText(locales.getDescripcion());
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public ImageView imgPreview;
        public TextView txtTitulo;
        public TextView txtDescripcion;

        public ViewHolder(View v) {
            super(v);
            //ButterKnife.bind(this, v);
            imgPreview = (ImageView)v.findViewById(R.id.imgPreview);
            txtTitulo = (TextView)v.findViewById(R.id.txtTitulo);
            txtDescripcion = (TextView)v.findViewById(R.id.txtDescripcion);
        }
    }
}
